import time
import datetime
import spidev

VREF = 2.048
ERR_INPUT = -1

class ads1220:
	def __init__(self):
		self.bus = 0
		self.device = 0

		self.dev = spidev.SpiDev()
		self.dev.open(self.bus, self.device)

		# settings
		self.dev.max_speed_hz = 250000
		self.dev.mode = 0b01
		self.dev.no_cs = True

	def init_config(self):
		""" Initialize configurations for Registers 0,1,2,3
		CMD:	0x43, WREG, 4 registers starting at address 0
		Reg0:	0x81, AINp = AIN0; AINn = AVSS; PGA dis
		Reg1:	0x40, DR = 90 SPS
		Reg2:	0x11, VREF = 2.048V; 50/60Hz filter; IDAC = 10uA
		Reg2:	0x10, VREF = 2.048V; 50/60Hz filter; IDAC = off
		Reg3:	0x00, IDAC1 dis; IDAC2 dis
		Reg3:	0x28, IDAC1 AIN0; IDAC2 AIN1
		Reg3:	0x2C, IDAC1 AIN0; IDAC2 AIN2
		"""
		tx = [0x43, 0x81, 0x40, 0x11, 0x2C]
		self.dev.writebytes(tx)

	def read_all_config_registers(self):
		""" Read all registers
		"""
		reg0 = self.read_register(0)
		reg1 = self.read_register(1)
		reg2 = self.read_register(2)
		reg3 = self.read_register(3)
		return reg0,reg1,reg2,reg3

	def read_register(self, ch):
		""" Read user defined register'
		CMD: RREG, read 1 byte starting from address ch
		"""
		if ch == 0:
			tx = [0x20, 0x00]
			rx = self.dev.xfer2(tx)
			return rx[1]
		elif ch == 1:
			tx = [0x24, 0x00]
			rx = self.dev.xfer2(tx)
			return rx[1]
		elif ch == 2:
			tx = [0x28, 0x00]
			rx = self.dev.xfer2(tx)
			return rx[1]
		elif ch == 3:
			tx = [0x2c, 0x00]
			rx = self.dev.xfer2(tx)
			return rx[1]
		else:
			return ERR_INPUT

	def adc_mux01(self):
		""" Configure DAC MUX
		CMD: 	0x40, RREG, start address 0, write 1 byte
		Reg0:	0x01, AINp = AIN0; AINn = AIN1; PGA dis
		"""
		tx = [0x40, 0x01]
		self.dev.writebytes(tx)

	def adc_mux23(self):
		""" Configure DAC MUX
		CMD: 	0x40, RREG, start address 0, write 1 byte
		Reg0:	0x51, AINp = AIN2; AINn = AIN3; PGA dis
		"""
		tx = [0x40, 0x51]
		self.dev.writebytes(tx)

	def adc_mux(self, ch):
		""" Configure DAC MUX
			AINp = ch
			AINn = AVSS
		"""
		if ch == 0:
			tx = [0x40, 0x81]
		elif ch == 1:
			tx = [0x40, 0x91]
		elif ch == 2:
			tx = [0x40, 0xA1]
		elif ch == 3:
			tx = [0x40, 0xB1]
		else:
			return ERR_INPUT

	def measure_adc01(self):
		""" Do measurement of AIN0 - AIN1
			1. Set the ADC to mux Ch0 and Ch1
			2. Start conversion
			3. Decode ADC data
		"""
		time.sleep(0.1)
		self.adc_mux01()
		time.sleep(0.1)
		self.do_conversion()
		time.sleep(0.1)
		adc_res = self.adc_measure()
		adc_dec = self.decode_adc_bytes(adc_res)
		adc_volt = self.convert_dec_to_volt(adc_dec)
		return adc_volt

	def measure_adc23(self):
		""" Do measurement of AIN2 - AIN3
			1. Set the ADC to mux Ch2 and Ch3
			2. Start conversion
			3. Decode ADC data
		"""
		time.sleep(0.1)
		self.adc_mux23()
		time.sleep(0.1)
		self.do_conversion()
		time.sleep(0.1)
		adc_res = self.adc_measure()
		adc_dec = self.decode_adc_bytes(adc_res)
		adc_volt = self.convert_dec_to_volt(adc_dec)
		return adc_volt

	def decode_adc_bytes(self):
		""" Convert measurement to decimal
		"""
		adc_dec = (adc_bytes[0] << 16) + (adc_bytes[1] << 8) + adc_bytes[2]
		return adc_dec

	def convert_dec_to_volt(self, adc_dec):
		""" Convert decimal to voltage
		"""
		volt = 2*VREF*adc_dec/2**24
		return volt

	def do_conversion(self):
		""" Begin ADC conversion of the measurement
		CMD: START/SYNC
		"""
		tx = [0x08]
		self.dev.writebytes(tx)

	def adc_measure(self):
		""" Grab measurement bytes
		CMD: RDATA, clock 3 bytes
		"""
		tx = [0x10, 0x00, 0x00, 0x00]
		return self.dev.xfer2(tx)

	def close(self):
		""" Close the device
		"""
		self.dev.close()

if __name__ == "__main__":
	print("Starting ADS1220 program...")
	ads = ads1220()

	time.sleep(0.5)
	ads.init_config()

	reg0,reg1,reg2,reg3 = ads.read_all_config_registers()

	print("Reg0: {}".format(reg0))
	print("Reg1: {}".format(reg1))
	print("Reg2: {}".format(reg2))
	print("Reg3: {}".format(reg3))


	adc_volt0 = ads.measure_adc01()
	adc_volt1 = ads.measure_adc23()

	print("Ch0 - Ch1: {}".format(adc_volt0))
	print("Ch2 - Ch3: {}".format(adc_volt1))

	ads.close()

	print("Program finshed!")