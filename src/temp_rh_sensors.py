import time
import datetime
import struct, array, io, fcntl

I2C_SLAVE = 0x0703

CMD_READ_TEMP_HOLD = "\xE3"
CMD_READ_TEMP_NOHOLD = "\xF3"
CMD_READ_HUM_HOLD = "\xE5"
CMD_READ_HUMNOHOLD = "\xF5"
CMD_SOFT_RESET = "\xFE"

class i2c(object):
    def __init__(self, device, bus):
        self.fr = io.open("/dev/i2c-"+str(bus), "rb", buffering=0)
        self.fw = io.open("/dev/i2c-"+str(bus), "wb", buffering=0)

        fcntl.ioctl(self.fr, I2C_SLAVE, device)
        fcntl.ioctl(self.fw, I2C_SLAVE, device)

    def write(self, bytes):
        self.fw.write(bytes)

    def read(self, bytes):
        return self.fr.read(bytes)

    def close(self):
        self.fw.close()
        self.fr.close()

class htu21d:
	def __init__(self):
		self.dev = i2c(0x40, 1)
        self.dev.write(CMD_SOFT_RESET)
        time.sleep(1)

	def measure_temp(self):
		""" Measure temp
		"""
		self.dev.write(CMD_READ_TEMP_HOLD)
        time.sleep(1)
        data = self.dev.read(3)
        buf = array.array('B', data)
        if self.crc8check(buf):
            return buf[0], buf[1]
        else:
            return -1, -1

	def measure_rh(self):
		""" Measure RH
		"""
		self.dev.write(self_READ_HUM_NOHOLD)
        time.sleep(1)
        data = self.dev.read(3)
        buf = array.array('B', data)
        if self.crc8check(buf):
            return buf[0], buf[1]
        else:
            return -1, -1

	def decode_temp(self, msb, lsb):
		""" Convert bytes to temp
		"""
		raw = (msb << 8) + lsb
		raw &= 0xFFFC
		temp = -46.85 + (175.72 * raw/65536)
		return temp

	def decode_rh(self):
		""" Convert bytes to rh
		"""
		raw = (msb << 8) + lsb
		raw &= 0xFFFC
		rh = -6 + (125.0*raw/65536)
		return rh

	def crc8check(self, value):
		""" CRC check
		"""
		remainder = ((value[0] << 8) + value[1]) << 8
		remainder |= value[2]
		divsor = 0x988000
		for i in range(0,16):
			if (remainder & 1 << (23-i)):
				remainder ^= divsor
			divsor = divsor >> 1
		if remainder == 0:
			return True
		else:
			return False

	def close(self):
		""" Close device
		"""
		self.dev.close()

if __name__ == "__main__":
	print("Starting RHT Program...")
    rht = HTU21D()

    msb, lsb = rht.measure_temp()
    temp = rht.decode_temp(msb, lsb)
    print("Temperature: {}".format(temp))

    msb, lsb = rht.measure_rh()
    rh = rht.decode_rh(msb, lsb)
    print("RH: {}".format(rh))

    rht.close()
    print("Program Finished!")