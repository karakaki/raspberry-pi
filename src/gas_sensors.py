import serial
import time
import datetime

class K30:
	def __init__(self):
		print("Initializing K-30 over UART")
		self.dev = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=2)
		self.co2 = 0
		time.sleep(1)
	
	def measure(self):
		self.dev.flushInput()
		self.dev.write(b'\xFE\x44\x00\x08\x02\x9F\x25')
		time.sleep(1)
		resp = self.dev.read(7)
		high = ord(resp[3])
		low = ord(resp[4])
		self.co2 = (high*256) + low
		return self.co2
	
if __name__ == "__main__":
	print("Starting Program...")

	cots = K30()
	res = cots.measure()

	print("CO2: {}".format(res))

  