import serial
import time
import datetime
from gas_sensors import K30
from adc import ads1220
from temp_rh_sensors import HTU21D

if __name__ == "__main__":
	print("Starting Main Program...")
	cots = K30()
	ads = ads1220()
	ads.init_config()
	ads.read_register_configs()
	rht = htu21d()
  
	time.sleep(1)	

	with open("running.txt", 'w') as file:
		file.write('1')
	
	while True:
		print("\n{}".format(datetime.datetime.now()))
		co2 = cots.measure()
		print("K-30 CO2: {}".format(co2))
		adc_sig = ads.measure_adc01()
		adc_ref = ads.measure_adc23()
		print("ADC SIG: {}".format(adc_sig))
		print("ADC REF: {}".format(adc_ref))
		msb, lsb = rht.measure_temp()
		if msb == -1:
			print("Temperature CRC check failed!")
		else:
			temp = rht.decode_temp(msb,lsb)
		msb, lsb = rht.measure_rh()
		if msb == -1:
			print("RH CRC check failed!")
		else:
			rh = rht.decode_rh(msb,lsb)
		print("Temperature: {}".format(temp))
		print("RH: {}".format(rh))

		# Check if we should still be running
		with open("running.txt", "r") as file:
			running = int(file.readline())
			if running == 0:
				print("\n\nUser request to quit program...")
				break

	time.sleep(3)

	ads.close()
	rht.close()
	print("Main Program Done!")